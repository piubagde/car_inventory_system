<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="js/jquery-ui.css">
    <link rel="stylesheet" href="js/jquery.dataTables.min.css">
    <link rel="stylesheet" href="bootstrap-5.0.0-beta3-dist/css/bootstrap.min.css">
    <script src="js/jquery.min.js"></script>
    <script src="js/jquery-ui.js"></script>
    <script src="js/jquery.dataTables.min.js"></script>
    <script src="bootstrap-5.0.0-beta3-dist/js/bootstrap.min.js"></script>
    <style type="text/css">
        #main
        {
        background-color:#c6ecc6;
        color:#ffffff;
    

        }


    </style>
</head>
<body id="main">
    <header class="fixed-top header bg-dark" style="height: 100px">
    	<div class="jumbotron text-center">
            <h2>Mini Car Inventory System</h2>
        </div>
            <div class="container">
                <nav class="navbar navbar-dark bg-dark">
            <!--span class="d-flex align-items-center justify-content-center" style="float: left;border:1px solid #c6ecc6; background-color: #c6ecc6;"-->
                    <div class="container-fluid">
                    
                     
                    </div>
                </nav>
            </div>
    </header>
    <div class="container">
        
        <div id="tabs" style="margin-top: 120px;background: #c6ecc6">
            <ul>
                <li><a href="#manufacturer">Add Manufacturer</a></li>
                <li><a href="#model" >Add Model</a></li>
                <li><a href="#inventory">View Inventory</a></li>
            </ul>
            <div id="inventory">
                <table id="inventory-table" class="display" style="width:100%">
                    <thead>
                        <tr>
                            <th>Serial Number</th>
                            <th>Manufacturer Name</th>
                            <th>Model Name</th>
                            <th>Count</th>
                        </tr>
                    </thead>
                </table>
                <!-- Trigger the modal with a button -->
                <button style="display:none;" id="model-button" type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Large Modal</button>
                <!-- Modal -->
                <div class="modal fade" id="myModal" role="dialog">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title"></h4>
                    </div>
                    <div class="modal-body">
                        <p></p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                    </div>
                </div>
                </div>
            </div>

            <div id="manufacturer">
                <fieldset class="form-control" >
                    <legend>Add Manufacturer</legend>
                    <br>
                    <div class="row mb-3">
                        <label class="col-sm-2 col-form-label "><strong>Manufacturer</strong></label>
                        <input class="col-sm-6 col-form-control" type="text" name="m_name" id="mname" autofocus/><br>
                    </div>
                    <br>
                    <br>
                    <div style="align-content: center; text-align: center;">
                        <button  value="Add" class="btn btn-primary" onclick="addManufacturer()">Add</button>
                        &nbsp;&nbsp;
                        <button type="reset" value="Clear" class="btn btn-primary" onclick="fun()">Reset</button>
                    </div>
                    <br>
                    <div class="container mb-3" style="border: 1px solid black; height: 20px">
                        <div id="alerts">
                        </div>
                    </div>
                </fieldset>
                <!--form action="#">
                    <div class="form-group">
                        <label for="manufacturer_name">Manufacturer:</label>
                        <input type="text" class="form-control" id="manufacturer_name" placeholder="Enter Manufacturer Name" name="manufacturer_name" required>
                    </div>
                    <div class="form-group text-center">
                        <button type="button" class="btn btn-default" onclick="addManufacturere()">Add</button>
                    </div>
                </form-->
            </div>
            <div id="model">
                <form id="model-form">
                    <div>
                        <div class="form-group">
                            <label for="model-name">Model Name:</label>
                            <input type="text" class="form-control input-sm" id="model-name">
                        <div>
                        <div class="form-group">
                            <label for="select-manufacturer">Select Manufacturer:</label>
                            <select class="form-control input-sm" id="select-manufacturer">
                            </select>
                        </div>
                    <div>
                    <div class="form-group">
                        <label for="model-color">Color:</label>
                        <input type="text" class="form-control input-sm" id="model-color">
                    <div>
                    <div class="form-group">
                        <label for="model-year">Manufacturing Year:</label>
                        <input type="text" class="form-control input-sm" id="model-year">
                    <div>
                    <div class="form-group">
                        <label for="model-reg-no">Registration Number:</label>
                        <input type="text" class="form-control input-sm" id="model-reg-no">
                    <div>
                    <div class="form-group">
                        <label for="model-note">Note:</label>
                        <input type="text" class="form-control input-sm" id="model-note">
                    <div>
                    <div class="form-group">
                        <label for="model-count">Count:</label>
                        <input type="number" class="form-control input-sm" id="model-count" min="0" value="0">
                    <div>
                    <div class="form-group">
                        <label for="image-1">Picture 1:</label>
                        <input type="file" name="image" id="image-1" required>
                    </div>
                    <div class="form-group">
                        <label for="image-2">Picture 2:</label>
                        <input type="file" name="image" id="image-2" required>
                    </div>
                    <div class="form-group text-center">
                        <button type="button" class="btn btn-default" onclick="addModel()">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    
</body>
<!--script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script-->
<script>
    function addManufacturer()
    {
        var mname=$('#mname').val().trim();
        if (mname=='') 
            {
                alert("please input Manufacturer name");
            }
        else
        {
            $.ajax({
                'url':"add_manufacturer.php",
                method:"POST",
                data:{'name': $('#mname').val().trim()},
                success:function(result)
                {
                    console.log(result);
                    console.log(typeof result);
                    //alert(result);
                    /*switch(result)
                    {
                        case 'success':
                        var msg = "Success! Manufacturer entry added successfully.";
                            break;
                        case "error":
                        var msg = "Failed! Something went wrong, Please try again later.";
                            break;
                        case "duplicate":
                            var msg = "Warning! Entry already exist.";
                            break;
                        default:
                          var msg="no msg";
                    }
                    console.log(msg);*/
                    $('#alerts').html(result);
                    $("#alerts").show();
                    $("#alerts").show().delay(5000).fadeOut();
                    document.getElementById('mname').value='';
            
                }
            });
        }
    }


            function fun(){
            document.getElementById('mname').value='';
           // document.getElementById('mname').autofocus=true;
            }

    $( function() {
        $( "#tabs" ).tabs();
    } );
    
</script>

</html>