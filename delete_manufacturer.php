		<?php 
		require("functions.php");
		$conn=getConn();


		$id=$_REQUEST['id'];
		$sql = $conn->prepare("DELETE FROM manufacturer WHERE id=:id");
		$sql->bindParam(':id', $id);
		$sql->execute();
		$result = $sql->setFetchMode(PDO::FETCH_ASSOC);
		
		if($result)
		{
		//$id=$conn->lastInsertId();
			header("Location: dashboard.php?msg=Record deleted successfully");

		} 
		else
		{
			header("Location: dashboard.php?msg=Something went wrong. Please try again later");
		}			
		?>
