<?php                                                 
require("functions.php");
$conn=getConn();

$name=$_REQUEST['name'];

$sql="SELECT COUNT(*) FROM manufacturer WHERE name='$name'";
$query=$conn->prepare($sql);
$query->execute();
$total_records=$query->fetchColumn();
if($total_records==0)
{
	$sql = $conn->prepare("INSERT INTO manufacturer (id, name, added_date, update_date) VALUES (NULL, :name, NOW(), NOW())");
	$sql->bindParam(':name', $name);
	$stmt=$sql->execute();
	$result = $sql->setFetchMode(PDO::FETCH_ASSOC);
			
	if($result)
	{
		flush();
		echo "<strong>Success!</strong> Manufacturer entry added successfully.";
		exit;
	} 
	else
	{
		flush();
		echo "Failed! Something went wrong, Please try again later.";
		exit;
	}
}                                                                           
else                                                                                                                                                           
{
	flush();
	echo "<strong>Warning!</strong> Entry already exist.";
	exit;
}



?>
